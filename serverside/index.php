<?php

use Phalcon\Loader;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;

$di = new FactoryDefault();

// Use Loader() to autoload our model
$loader = new Loader();
$loader->registerDirs(
    array(
        '/var/www/peak/models/'
    )
)->register();

// Set up the database service
$di->set('db', function () {
    return new PdoMysql(
        array(
            "host"     => "localhost",
            "username" => "root",
            "password" => "er3heat",
            "dbname"   => "peak",
            "charset" => "utf8"
        )
    );
});

// Create and bind the DI to the application
$app = new Micro($di);

$app->get('/api/meta', function () use ($app, $di) {
	

    $routes = [];

    $router = $di->get('router');
    foreach ($router->getRoutes() as $route) {
        /** @var \Phalcon\Mvc\Router\Route $route */
        $paths = $route->getPaths();

        $pathsString = '';

        if (isset($paths['module'])) {
            $pathsString .= $paths['module'];
        }

        if (isset($paths['namespace'])) {
            $pathsString .= '\\' . ltrim($paths['namespace'], '\\');
        }

        if (isset($paths['controller'])) {
            $pathsString .= '\\' . Text::camelize(ltrim($paths['controller'], '\\'));
        }

        if (isset($paths['action'])) {
            $pathsString .= '::' . $paths['action'];
        }

        $routes[$route->getRouteId()] = (object) [
            'name'             => $route->getName(),
            'http_methods'     => $route->getHttpMethods(),
            'host_name'        => $route->getHostname(),
            'pattern'          => $route->getPattern(),
            'before_match'     => $route->getBeforeMatch(),
            'compiled_pattern' => $route->getCompiledPattern(),
            'group'            => $route->getGroup(),
            'converters'       => $route->getConverters(),
            'paths'            => $pathsString,
        ];
    }
    
    echo "<h3>Available API routes:<br></h3>";
    echo "<ul>";
    foreach($routes as $route){        
        echo "<li>".$route->http_methods.": ".$route->pattern."</li>";
    }
    echo "</ul>";
});


$app->get('/find/{table}/{field}/{value}/{limit}/{order_by}/{order}', function ($table, $field, $value, $limit, $order_by, $order) use ($app) {
    
    $model = modelFromTableName($table);
    
    $data = $model::find(
        array(
            "$field = '$value'",
            "order" => "$order_by $order",
            "limit" => $limit
        )
    );
    
    return getJsonResponse($data);
});


$app->get('/get_single/{table}/{id}', function ($table, $id) use ($app) {
    
    $model = modelFromTableName($table);
    
    if($model != false){
        $data = $model::findFirst($id);
    } else {
        $data = null;
    }
    return getJsonResponse($data);
});

$app->get('/get_multiple/{table}/{limit}/{order_by}/{order}', function ($table, $limit, $order_by, $order) use ($app) {
    
    $model = modelFromTableName($table);
    
    if($model != null){
        $data = $model::find(
            array(
                "order" => "$order_by $order",
                "limit" => $limit
            )
        );
    } else {
        $data = null;
    }
    return getJsonResponse($data);
});

$app->get('/get_fat/summit/{id}', function ($id) use ($app) {

    $summit = Summit::findFirst($id);
    $summit_array = $summit->toArray();
    
    $images = $summit->getSummitImage()[0];
    $image_array = $images->toArray();
    
    $ratings = UserSummitRating::find(
        array(
            "summit_id = $id",
        )
    );
    $rating_array = $ratings->toArray();
    
    $comments = UserSummitComment::find(
        array(
            "summit_id = $id",
        )
    );
    $comment_array = $comments->toArray();
    
    $rating;
    $difficulty;
    $rating_sum = 0;
    $difficulty_sum = 0;
    $count = count($rating_array);
    
    for($i = 0; $i < $count; $i++){
        $rating_sum += $rating_array[$i]["rating"];
        $difficulty_sum += $rating_array[$i]["difficulty"];
    }
    
    if($rating_sum == 0){
        $rating = 0;
    } else {
        $rating = $rating_sum / $count;
    }
    
    if($difficulty_sum == 0){
        $difficulty = 0;
    } else {
        $difficulty = $difficulty_sum / $count;
    }
    
    $data = array('summit' => $summit_array, 'image' => $image_array, 'comments' => $comment_array,'rating' => $rating, 'difficulty' => $difficulty );
    
    $response = new \Phalcon\Http\Response();
    $response->setHeader("Content-Type", "application/json");
    $response->setContent(json_encode($data));
    return $response;
});


$app->get('/summit/{id}/route/{route_id}/geopoint_all', function ($id, $route_id) use ($app) {
    
    $summit = Summit::findFirst($id);
    if($summit != false){
        $data = $summit->SummitRoute[(int)$route_id]->RouteGeopoint;
    } 
    return getJsonResponse($data);
});


$app->get('/get_multiple_children/{table}/{id}/{table_child}/{limit}/{order_by}/{order}', function ($table, $id, $table_child, $limit, $order_by, $order) use ($app) {
    
    $model = modelFromTableName($table_child);
    
    if($model != false){
        $data = $model::find(
            array(
                $table."_id = $id",
                "order" => "$order_by $order",
                "limit" => $limit
            )
        );
    } else {
        $data = null;
    }
    return getJsonResponse($data);
});

$app->get('/get_single_child/{table}/{id}/{child_table}/{child_id}', function ($table, $id, $child_table, $child_id) use ($app) {
    
    $model = modelFromTableName($table);
    
    $parent = $model::findFirst($id);
    if($parent!=false){     
        $model_name = get_class(modelFromTableName($child_table));
        $data = $parent->$model_name;
        
        $key = (int)$child_id;             
        if(isset($data[$key]))
            $data = $data[$key];
        else 
            return getJsonResponse(null);
    }
    return getJsonResponse($data);
});



/********************* INSERTS *************************************************/

$app->post('/insert/{table}', function ($table) use ($app) {
    
    $response = new Response();
    
    $model = modelFromTableName($table);
    
    try {
        $data = $app->request->getJsonRawBody(); 

        foreach($data as $key => $value){
            $model->$key = $value;
        }     
        
        $model->save();
        
        if($model){
            return getJsonId($model);           
        }
        else {
            $response->setStatusCode(409, "Conflict");
        }
    }
    catch(Exception $e){
        $response->setStatusCode(409, "Conflict");
    }
    
});

function getJsonResponse($data){
    
    if($data != null && $data != false){
        if(is_object($data))
            $data = $data->toArray();           
    }
    else {
        $data = json_decode ("{}");   
    }
    
    $response = new \Phalcon\Http\Response();
    $response->setHeader("Content-Type", "application/json");
    $response->setContent(json_encode($data));
    return $response;
}

function getJsonId($data){
    if($data){
        return getJsonResponse(array("id"=>$data->id,"uuid"=>$data->uuid));
    } else {
        return getJsonResponse(null);
    }
}

function modelFromTableName($table){
    $model = null;
    
    switch($table){
        case "summit":
            $model = new Summit();
            break;          
        case "summit_image":
            $model = new SummitImage();
            break;
        case "summit_route":
            $model = new SummitRoute();
            break;      
        case "user":
            $model = new User();
            break;      
        case "user_summit_comment":
            $model = new UserSummitComment();
            break;
        case "user_summit_rating":
            $model = new UserSummitRating();
            break;
        case "route_geopoint":
            $model = new RouteGeopoint();
            break;                  
        default:
            $model = null;
    }
    
    return $model;
}


$app->handle();