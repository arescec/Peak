<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class SummitRoute extends Model
{
	public $id;
	public $uuid;
	public $difficulty;
	public $rating;
	public $time;
	public $created_at;
	public $updated_at;
	public $name;
	public $description;
	public $summit_id;
	
	public function initialize(){
		$this->belongsTo("summit_id", "Summit", "id");
		$this->hasMany("id", "RouteGeopoint", "summit_route_id");
	}
}
