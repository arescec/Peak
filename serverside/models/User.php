<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class User extends Model
{
	public $id;
	public $uuid;
	public $name;
	public $surname;
	public $social_id;
	public $network;
	public $email;
	public $created_at;
	public $updated_at;
	
	public function initialize(){
		$this->hasMany("id", "Summit", "user_id");
		$this->hasMany("id", "UserSummitRating", "user_id");
		$this->hasMany("id", "UserSummitComment", "user_id");
	}
}
