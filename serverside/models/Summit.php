<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class Summit extends Model
{
	public $id;
	public $uuid;
	public $name;
	public $lat;
	public $lng;
	public $description;
	public $height;
	public $difficulty;
	public $rating;
	public $created_at;
	public $updated_at;
	public $user_id;
	
	public function initialize(){
		$this->hasMany("id", "SummitImage", "summit_id");
		$this->hasMany("id", "SummitRoute", "summit_id");
		$this->hasMany("id", "UserSummitComment", "summit_id");
		$this->hasMany("id", "UserSummitRating", "summit_id");
		$this->belongsTo("user_id", "User", "id");
	}
}
