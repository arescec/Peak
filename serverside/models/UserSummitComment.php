<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class UserSummitComment extends Model
{
	public $id;
	public $uuid;
	public $text;
	public $title;
	public $created_at;
	public $updated_at;
    public $user_id;
	public $summit_id;
	
	public function initialize(){
		$this->belongsTo("summit_id", "Summit", "id");
		$this->belongsTo("user_id", "User", "id");
	}
}