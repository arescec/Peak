<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class RouteGeopoint extends Model
{
	public $id;
	public $uuid;
	public $lat;
    public $lng;
	public $created_at;
	public $updated_at;
	public $summit_route_id;
    public $summit_route_summit_id;
	
	public function initialize(){
		$this->belongsTo("summit_route_id", "SummitRoute", "id");
	}
}
