<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;

class SummitImage extends Model
{
	public $id;
	public $uuid;
	public $data;
	public $created_at;
	public $updated_at;
	public $summit_id;
	public $user_id;
	
	public function initialize(){
		$this->belongsTo("summit_id", "Summit", "id");
	}
}
