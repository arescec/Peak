package com.nwh.peak;

import android.content.Context;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;

class GeoPointContainer {

    private ArrayList<GeoPoint> geoPoints;
    private Context context;

    public GeoPointContainer(ArrayList<GeoPoint> geoPoints, Context context){
        this.geoPoints = geoPoints;
        this.context = context;
    }

    public ArrayList<GeoPoint> getGeoPoints(){
        return this.geoPoints;
    }

    public Context getContext(){
        return this.context;
    }
}