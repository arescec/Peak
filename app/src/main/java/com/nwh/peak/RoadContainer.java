package com.nwh.peak;


import android.content.Context;

import org.osmdroid.bonuspack.routing.Road;

class RoadContainer {
    private Context context;
    private Road road;

    public RoadContainer(Road road, Context context){
        this.context = context;
        this.road = road;
    }

    public Context getContext(){
        return this.context;
    }

    public Road getRoad(){
        return this.road;
    }
}