package com.nwh.peak;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Comment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CommentActivity extends AppCompatActivity {

    Button submitButton;
    EditText commentInput;
    UserSummitCommentModel comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        submitButton = (Button)findViewById(R.id.buttonCommentSubmit);
        commentInput = (EditText)findViewById(R.id.textCommentContent);

        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                User user = PrefUtils.getCurrentUser(CommentActivity.this);

                comment = new UserSummitCommentModel();
                comment.setText(commentInput.getText().toString());
                comment.setTitle(user.name);
                comment.setSummit_id(getIntent().getIntExtra("KEY_SUMMIT_ID", 1));
                comment.setUser_id(1); // ?!

                Helper.getPeakApi(CommentActivity.this).postSummitComment(comment, new Callback<UserSummitCommentModel>() {

                    @Override
                    public void success(UserSummitCommentModel comment, Response response) {
                        Toast.makeText(CommentActivity.this, "Comment posted", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.getLocalizedMessage();
                        Toast.makeText(CommentActivity.this, "Error: Comment not posted", Toast.LENGTH_LONG).show();
                    }

                });

                finish();
            }
        });
    }

}
