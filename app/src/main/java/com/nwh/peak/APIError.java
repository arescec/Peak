package com.nwh.peak;

public class APIError {

    Error error;

    public static class Error {
        Data data;

        public static class Data {
            String message;
        }
    }
}