package com.nwh.peak;

/**
 * Created by Administrator on 23.4.2016..
 */
public class SummitRouteModel {

    private int id;
    private String uuid;
    private String data;
    private float difficulty;
    private float rating;
    private float time;
    private String created_at;
    private String updated_at;
    private String name;
    private String description;
    private int summit_id;
    private int user_id;

}
