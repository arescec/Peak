package com.nwh.peak;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Base64;
import android.util.Log;

import com.squareup.okhttp.ResponseBody;

import java.io.ByteArrayOutputStream;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.location.Location.distanceBetween;

/**
 * Created by Administrator on 19.4.2016..
 */
public class Helper {

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap base64ToBitmap (String codedImage, Context ctx){
        byte[] decodedString;
        Bitmap imageBitmap;

        if(codedImage != null){
            decodedString = Base64.decode(codedImage, Base64.DEFAULT);
            imageBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } else {
            imageBitmap = Helper.drawableToBitmap(ContextCompat.getDrawable(ctx, R.drawable.no_image));
        }

        return imageBitmap;
    }

    public static String bitmapTobase64 (Bitmap bitmap){

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static PeakAPI getPeakApi(Context c){

        RestAdapter adapter;

        final String ROOT_URL = "http://188.166.1.74/";
        adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .setErrorHandler(new APIErrorHandler(c))
                .build();

        return adapter.create(PeakAPI.class);

    }

}
