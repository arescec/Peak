package com.nwh.peak;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;


import java.util.ArrayList;

import static android.location.Location.distanceBetween;

public class MapActivity extends AppCompatActivity {

    private LocationManager mLocMgr;
    private ItemizedOverlay<OverlayItem> mMyLocationOverlay;
    private ResourceProxy mResourceProxy;

    private  GPSTracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        double lat = (double)getIntent().getFloatExtra("KEY_SUMMIT_LAT", 0);
        double lng = (double)getIntent().getFloatExtra("KEY_SUMMIT_LNG", 0);

        MapView map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(10);
        GeoPoint summitPoint = new GeoPoint(lat, lng);

        tracker = new GPSTracker(MapActivity.this);
        GeoPoint currentPoint = new GeoPoint(tracker.getLatitude(), tracker.getLongitude());

        if(GPSTracker.positionKnown){
            mapController.setCenter(currentPoint);
        }
        else {
            mapController.setCenter(summitPoint);
        }

        Marker summitMarker = new Marker(map);
        summitMarker.setIcon(ContextCompat.getDrawable(this, R.drawable.red_flag_marker));
        summitMarker.setTitle("Summit");
        summitMarker.setPosition(summitPoint);
        summitMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        map.getOverlays().add(summitMarker);

        if(GPSTracker.positionKnown){
            Marker currentMarker = new Marker(map);
            currentMarker.setIcon(ContextCompat.getDrawable(this, R.drawable.red_map_marker));
            currentMarker.setTitle("Current location");
            currentMarker.setPosition(currentPoint);
            currentMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            map.getOverlays().add(currentMarker);

            ArrayList<GeoPoint> waypoints = new ArrayList<>();
            waypoints.add(currentPoint);
            waypoints.add(summitPoint);

            GeoPointContainer geoPointContainer = new GeoPointContainer(waypoints, this);

            new RouteToSummit().execute(geoPointContainer);
        }

    }

}
