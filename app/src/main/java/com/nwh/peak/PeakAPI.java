package com.nwh.peak;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Belal on 11/3/2015.
 */
public interface PeakAPI {

    @POST("/insert/summit_image")
    void postSummitImage(@Body SummitImageModel task, Callback<SummitImageModel> cb);

    @POST("/insert/user_summit_comment")
    void postSummitComment(@Body UserSummitCommentModel task, Callback<UserSummitCommentModel> cb);

    @GET("/get_multiple/summit/12/created_at/DESC")
    void getSummits(Callback<List<SummitModel>> response);

    @GET("/get_fat/summit/{id}")
    void getFatSummit(@Path("id") int id, Callback<FatSummitModel> response);

    @GET("/get_single/user/{id}")
    void getSingleUser(@Path("id") int user_id, Callback<UserModel> response);

    @GET("/get_single/summit/{id}")
    void getSummit(@Path("id") int id, Callback<SummitModel> response);

    @GET("/get_single_child/summit/{summit_id}/summit_image/{id}")
    void getSummitImage(@Path("summit_id") int summit_id, @Path("id") int id, Callback<SummitImageModel> response);

    @GET("/get_multiple_children/summit/{summit_id}/summit_image/{limit}/created_at/DESC")
    void getSummitImages(@Path("summit_id") int summit_id, @Path("limit") int limit, Callback<List<SummitImageModel>> response);

    @GET("/get_multiple_children/summit/{summit_id}/user_summit_comment/{limit}/created_at/DESC")
    void getSummitComments(@Path("summit_id") int summit_id, @Path("limit") int limit, Callback<List<UserSummitCommentModel>> response);

}