package com.nwh.peak;

import java.io.Serializable;

public class Route implements Serializable
{
    private int id;
    private String name;
    private String description;
    private float difficulty;
    private float approx_time;
    private float lat;
    private float lng;

    public int getId(){ return this.id; }
    public String getName(){ return this.name; }
    public String getDescription(){ return  this.description; }
    public float getDifficulty(){ return this.difficulty; }
    public float getApprox_time(){ return this.approx_time; }
    public float getLat(){ return this.lat; }
    public float getLng(){ return this.lng; }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", description = "+description+", name = "+name+", lng = "+lng+", difficulty = "+difficulty+", approx_time = "+approx_time+", lat = "+lat+"]";
    }
}
