package com.nwh.peak;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    public List<SummitModel> summits;
    public List<SummitImageModel> imageList;
    public static final String ROOT_URL = "http://188.166.1.74/";
    private RecyclerView recyclerView;
    private GPSTracker tracker;
    private int iterator;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        tracker = new GPSTracker(MainActivity.this);

        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new PermissionsResultAction() {

                    @Override
                    public void onGranted() {
                        tracker.updateGPSCoordinates();
                    }

                    @Override
                    public void onDenied(String permission) {
                        Toast.makeText(MainActivity.this, "Nope, can't do it without permissions!", Toast.LENGTH_LONG).show();
                    }
                });

        user = PrefUtils.getCurrentUser(MainActivity.this);
        facebookSdkInit();

        imageList = new ArrayList<>();
        getSummits();

    }

    public void facebookSdkInit(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.nwh.peak",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_photo).setVisible(false);
        menu.findItem(R.id.action_comment).setVisible(false);

        if(user != null){
            menu.findItem(R.id.action_login).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(true);
        } else {
            menu.findItem(R.id.action_login).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_login:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void getSummits(){
        Log.d("nwh", "Getting summits");

        Helper.getPeakApi(this).getSummits(new Callback<List<SummitModel>>() {

            @Override
            public void success(List<SummitModel> list, Response response) {

                summits = list;

                if (!summits.isEmpty()) {
                    iterator = 0;
                    getImage();
                } else {
                    setContentView(R.layout.empty);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
            }
        });
    }

    private void getImage(){

        Helper.getPeakApi(this).getSummitImage(summits.get(iterator).getId(), 0, new Callback<SummitImageModel>() {

            @Override
            public void success(SummitImageModel image, Response response) {
                if (image != null) {
                    imageList.add(image);
                } else {
                    imageList.add(new SummitImageModel());
                }

                iterator++;

                if(iterator < summits.size()){
                    getImage();
                } else {
                    setContentView(R.layout.activity_main);

                    //EventBus.getDefault().register(this);

                    recyclerView = (RecyclerView) findViewById(R.id.summitRecyclerView);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                    SummitAdapter summitAdapter = new SummitAdapter(summits, imageList, getApplicationContext());
                    // 4. set adapter
                    recyclerView.setAdapter(summitAdapter);
                    // 5. set item animator to DefaultAnimator
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                }

            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
            }
        });

    }

}