package com.nwh.peak;

import android.content.Context;
import android.util.Log;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

public class APIErrorHandler implements ErrorHandler {
    private Context ctx;

    public APIErrorHandler(Context c) {
        this.ctx = c;
    }

    @Override
    public Throwable handleError(RetrofitError cause) {
        String errorDescription;

        if (cause.isNetworkError()) {
            errorDescription = ctx.getString(R.string.error_network);
        } else {
            if (cause.getResponse() == null) {
                errorDescription = ctx.getString(R.string.error_no_response);
            } else {

                // Error message handling - return a simple error to Retrofit handlers..
                try {
                    APIError errorResponse = (APIError) cause.getBodyAs(APIError.class);
                    errorDescription = errorResponse.error.data.message;
                } catch (Exception ex) {
                    try {
                        errorDescription = ctx.getString(R.string.error_network_http_error, cause.getResponse().getStatus());
                    } catch (Exception ex2) {
                        Log.e("nwh", "handleError: " + ex2.getLocalizedMessage());
                        errorDescription = ctx.getString(R.string.error_unknown);
                    }
                }
            }
        }

        return new Exception(errorDescription);
    }
}