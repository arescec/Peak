package com.nwh.peak;

import android.app.Activity;
import android.os.AsyncTask;

import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.views.MapView;

class RouteToSummit extends AsyncTask<GeoPointContainer, Void, RoadContainer> {

    protected RoadContainer doInBackground(GeoPointContainer... containers) {

        RoadManager roadManager = new OSRMRoadManager(containers[0].getContext());
        Road road = roadManager.getRoad(containers[0].getGeoPoints());

        RoadContainer roadContainer = new RoadContainer(road, containers[0].getContext());
        return roadContainer;

    }

    protected void onPostExecute(RoadContainer roadContainer) {

        MapView map = (MapView)((Activity) roadContainer.getContext()).findViewById(R.id.map);

        Polyline roadOverlay = RoadManager.buildRoadOverlay(roadContainer.getRoad(), roadContainer.getContext());
        map.getOverlays().add(roadOverlay);
        map.invalidate();
    }
}