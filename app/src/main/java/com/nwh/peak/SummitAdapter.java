package com.nwh.peak;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class SummitAdapter extends RecyclerView.Adapter<SummitAdapter.ViewHolder> {

    private List<SummitModel> summits;
    private List<SummitImageModel> summitImages;

    private View itemLayoutView;
    private Context context;

    public SummitAdapter(List<SummitModel> summits, List<SummitImageModel> summitImages, Context context){
        this.context = context;
        this.summits = summits;
        this.summitImages = summitImages;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SummitAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_summit_item, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SummitDetails.class);
                intent.putExtra("KEY_SUMMIT_ID", (int)summits.get(position).getId());
                view.getContext().startActivity(intent);
            }
        });

        byte[] decodedString;
        Bitmap imageBitmap;

        if(summitImages != null && position < summitImages.size()){
            imageBitmap = Helper.base64ToBitmap(summitImages.get(position).getData(), context);
        }
        else {
            imageBitmap = Helper.drawableToBitmap(ContextCompat.getDrawable(context, R.drawable.no_image));
        }

        viewHolder.imageViewSummitThumbnail.setImageBitmap(imageBitmap);

        viewHolder.textViewSummitCardName.setText(summits.get(position).getName());


        if(GPSTracker.positionKnown){
            viewHolder.textViewSummitDistance.setText(Integer.toString(summits.get(position).getDistance()) + " km");
        }
        else {
            viewHolder.textViewSummitDistance.setText("? km");
        }

        viewHolder.textViewSummitDifficulty.setText(Integer.toString((int)summits.get(position).getDifficulty() * 10));
        viewHolder.textViewSummitDifficulty.setText(Integer.toString((int)summits.get(position).getRating() * 10));
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout cardTextBackground;
        public TextView textViewSummitCardName;
        public TextView textViewSummitCardDescription;
        public TextView textViewSummitDistance;
        public TextView textViewSummitDifficulty;
        public TextView textViewSummitRating;
        public ImageView imageViewSummitThumbnail;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            cardTextBackground = (LinearLayout) itemLayoutView.findViewById(R.id.cardTextBackground);
            textViewSummitCardName = (TextView) itemLayoutView.findViewById(R.id.summitCardName);
            textViewSummitDistance = (TextView) itemLayoutView.findViewById(R.id.summitCardDistanceText);
            textViewSummitDifficulty = (TextView) itemLayoutView.findViewById(R.id.summitCardDifficultyText);
            textViewSummitRating = (TextView) itemLayoutView.findViewById(R.id.summitCardRatingText);
            imageViewSummitThumbnail = (ImageView) itemLayoutView.findViewById(R.id.summitCardThumbnail);

            //imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.summitCardThumbnail);
        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(summits != null)
            return summits.size();
        else {
            return 0;
        }
    }

}