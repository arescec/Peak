package com.nwh.peak;

/**
 * Created by Administrator on 26.4.2016..
 */
public class FatSummitModel {

    private SummitModel summit;
    private SummitImageModel image;
    private float rating;
    private float difficulty;

    public SummitModel getSummit() {
        return summit;
    }

    public void setSummit(SummitModel summit) {
        this.summit = summit;
    }

    public SummitImageModel getImage() {
        return image;
    }

    public void setImage(SummitImageModel image) {
        this.image = image;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

}
