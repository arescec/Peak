package com.nwh.peak;

import java.io.Serializable;

public class RouteWrapper implements Serializable {
    private Route[] routes;

    public RouteWrapper(Route[] routes) {
        this.routes = routes;
    }

    public Route[] getRoutes() {
        return this.routes;
    }

}