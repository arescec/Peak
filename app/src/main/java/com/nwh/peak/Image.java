package com.nwh.peak;

import java.io.Serializable;

public class Image implements Serializable
{
    private String data;

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+"]";
    }
}
