package com.nwh.peak;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CommentAdapter extends ArrayAdapter<CommentContainer> {
    private final Context context;
    private final ArrayList<CommentContainer> comments;

    public CommentAdapter(Context context, ArrayList<CommentContainer> comments) {
        super(context, -1, comments);
        this.context = context;
        this.comments = comments;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.details_comment_item, parent, false);


        TextView commentHeader = (TextView) rowView.findViewById(R.id.detailsCommentHeader);
        TextView commentContent = (TextView) rowView.findViewById(R.id.detailsCommentContent);
        ImageView commentIcon = (ImageView) rowView.findViewById(R.id.detailsCommentIcon);

        commentHeader.setText(comments.get(position).getTitle());
        commentContent.setText(comments.get(position).getText());
        commentIcon.setImageBitmap(Helper.base64ToBitmap(comments.get(position).getImage(), context));


        return rowView;
    }
}

