package com.nwh.peak;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.loader.DefaultImageLoader;
import com.veinhorn.scrollgalleryview.loader.MediaLoader;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GalleryActivity extends FragmentActivity {

    private ScrollGalleryView scrollGalleryView;
    private List<SummitImageModel> image_list_64;
    private Bitmap[] images;
    private int summit_id;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);


        //Retrieve images from intent
        summit_id = (int) getIntent().getIntExtra("KEY_SUMMIT_ID", 0);

        Helper.getPeakApi(this).getSummitImages(summit_id, 20, new Callback<List<SummitImageModel>>() {

            @Override
            public void success(List<SummitImageModel> list, Response response) {
                setContentView(R.layout.activity_gallery);

                image_list_64 = list;

                if(image_list_64 != null){
                    images = new Bitmap[image_list_64.size()];

                    for (int i = 0; i < image_list_64.size(); i++) {
                        images[i] = Helper.base64ToBitmap(image_list_64.get(i).getData(), GalleryActivity.this);
                    }


                    List<MediaInfo> infos = new ArrayList<>(images.length);

                    scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);
                    scrollGalleryView
                            .setThumbnailSize(180)
                            .setZoom(true)
                            .setFragmentManager(getSupportFragmentManager());

                    for (int i = 0; i < image_list_64.size(); i++) {
                        scrollGalleryView.addMedia(MediaInfo.mediaLoader(new DefaultImageLoader(images[i])));
                    }

                    scrollGalleryView.addMedia(infos);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
            }
        });

    }

    private Bitmap toBitmap(int image) {
        return ((BitmapDrawable) getResources().getDrawable(image)).getBitmap();
    }
}