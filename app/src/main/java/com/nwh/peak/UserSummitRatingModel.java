package com.nwh.peak;

/**
 * Created by Administrator on 23.4.2016..
 */
public class UserSummitRatingModel {

    private int id;
    private String uuid;
    private float rating;
    private float difficulty;
    private String created_at;
    private String updated_at;
    private int user_id;
    private int summit_id;

    public float getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSummit_id() {
        return summit_id;
    }

    public void setSummit_id(int summit_id) {
        this.summit_id = summit_id;
    }

}
