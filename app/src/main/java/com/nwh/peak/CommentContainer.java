package com.nwh.peak;

/**
 * Created by Administrator on 26.4.2016..
 */
public class CommentContainer {

    private String username;
    private String text;
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public CommentContainer(String title, String text, String image) {
        this.title = title;
        this.text = text;
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
