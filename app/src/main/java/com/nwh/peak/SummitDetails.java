package com.nwh.peak;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Rating;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.facebook.FacebookActivity;

import junit.framework.Test;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Path;

public class SummitDetails extends AppCompatActivity {

    public List<SummitModel> summits;
    public static final String ROOT_URL = "http://188.166.1.74/";

    private ImageView imageViewSummitThumbnail;
    private Bitmap image;
    private TextView textViewSummitDescription;
    private Button buttonDetailsMap;
    private Button buttonDetailsGallery;
    private Button buttonAddComment;
    private RatingBar ratingBar;
    private RatingBar difficultyBar;
    private ScrollView scrollView;
    private ArrayList<UserSummitCommentModel> comments;
    private ArrayList<UserModel> users;
    private ArrayList<CommentContainer> commentContainers;
    private int summit_id;
    private int user_id;
    private int iterator;
    private float rating;
    private float difficulty;
    private User user;

    private SummitModel summit;
    public static final int CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE = 3306;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        user = PrefUtils.getCurrentUser(SummitDetails.this);

        users = new ArrayList<>();
        summit_id = getIntent().getIntExtra("KEY_SUMMIT_ID", 1);
        commentContainers = new ArrayList<>();
        iterator = 0;
        user_id = 1;

        getSummit(summit_id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        if(user != null){
            menu.findItem(R.id.action_login).setVisible(false);
            menu.findItem(R.id.action_logout).setVisible(true);
        } else {
            menu.findItem(R.id.action_login).setVisible(true);
            menu.findItem(R.id.action_logout).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_login:
                startActivity(new Intent(SummitDetails.this, LoginActivity.class));
                break;

            case R.id.action_logout:
                startActivity(new Intent(SummitDetails.this, LoginActivity.class));
                break;

            case R.id.action_photo:
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this,
                        new String[]{Manifest.permission.CAMERA}, new PermissionsResultAction() {

                            @Override
                            public void onGranted() {
                                Intent photo_intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                startActivityForResult(photo_intent, CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE);
                            }

                            @Override
                            public void onDenied(String permission) {
                                Toast.makeText(SummitDetails.this, "Nope, can't do it without permissions!", Toast.LENGTH_LONG).show();
                            }
                        });

            case R.id.action_comment:
                Intent intent = new Intent(SummitDetails.this, CommentActivity.class);
                intent.putExtra("KEY_SUMMIT_ID", summit_id);
                intent.putExtra("KEY_USER_ID", summit_id);
                startActivity(intent);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //Check that request code matches ours:
        if (requestCode == CAPTURE_IMAGE_THUMBNAIL_ACTIVITY_REQUEST_CODE) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            if (bitmap != null) {
                bitmap = scaleDown(bitmap, 1920, true);
                imageViewSummitThumbnail.setImageBitmap(bitmap);

                SummitImageModel summitImage = new SummitImageModel();
                summitImage.setData(Helper.bitmapTobase64(bitmap));
                summitImage.setUser_id(user_id);
                summitImage.setSummit_id(summit_id);

                Toast.makeText(SummitDetails.this, "Uploading image...", Toast.LENGTH_LONG).show();

                Helper.getPeakApi(this).postSummitImage(summitImage, new Callback<SummitImageModel>(){

                    @Override
                    public void success(SummitImageModel summitImageModel, Response response) {
                        Toast.makeText(SummitDetails.this, "Image uploaded", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.getLocalizedMessage();
                        Toast.makeText(SummitDetails.this, "Image upload failed", Toast.LENGTH_LONG).show();
                    }

                });
            }

        }
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    private void startCommentActivity(){
        Intent intent = new Intent(SummitDetails.this, CommentActivity.class);
        intent.putExtra("KEY_SUMMIT_ID", summit_id);
        intent.putExtra("KEY_USER_ID", summit_id);
        startActivity(intent);
    }

    private void setViews() {

        setContentView(R.layout.activity_summit_details);
        imageViewSummitThumbnail = (ImageButton) findViewById(R.id.imageViewSummitThumbnail);
        imageViewSummitThumbnail.setImageBitmap(image);

        buttonAddComment = (Button) findViewById(R.id.detailsAddComment);
        scrollView = (ScrollView) findViewById(R.id.detailsScrollView);
        textViewSummitDescription = (TextView) findViewById(R.id.textViewSummitDescription);
        buttonDetailsMap = (Button) findViewById(R.id.buttonDetailsMap);
        buttonDetailsGallery = (Button) findViewById(R.id.buttonDetailsGallery);
        ratingBar = (RatingBar) findViewById(R.id.rating);
        difficultyBar = (RatingBar) findViewById(R.id.difficulty);

        setTitle(summit.getName());
        textViewSummitDescription.setText(summit.getDescription());
        ratingBar.setRating(summit.getRating());
        difficultyBar.setRating(summit.getDifficulty());

        byte[] decodedString;
        Bitmap imageBitmap;
        Bitmap resizedImage;


        buttonDetailsMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MapActivity.class);
                intent.putExtra("KEY_SUMMIT_LAT", summit.getLat());
                intent.putExtra("KEY_SUMMIT_LNG", summit.getLng());
                v.getContext().startActivity(intent);
            }
        });

        buttonDetailsGallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SummitDetails.this, GalleryActivity.class);
                intent.putExtra("KEY_SUMMIT_ID", summit_id);
                startActivity(intent);
            }
        });


        buttonAddComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            startCommentActivity();
            }
        });
    }

    private void getSummit(int id){

        //Defining the method
        Helper.getPeakApi(this).getSummit(id, new Callback<SummitModel>() {

            @Override
            public void success(SummitModel s, Response response) {
                summit = s;
                getImage(summit.getId());
            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
                Toast.makeText(SummitDetails.this, "Connection error", Toast.LENGTH_SHORT);
            }
        });
    }


    private void getImage(final int summit_id){

        Helper.getPeakApi(this).getSummitImage(summit_id, 0, new Callback<SummitImageModel>() {

            @Override
            public void success(SummitImageModel i, Response response) {
                if(i.getData() == null){
                    buttonDetailsGallery.setEnabled(false);
                }
                image = Helper.base64ToBitmap(i.getData(), SummitDetails.this);

                getComments(summit_id);
                setViews();
            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
                Toast.makeText(SummitDetails.this, "Connection error", Toast.LENGTH_SHORT);
            }
        });
    }


    private void getComments(int summit_id){

        Helper.getPeakApi(this).getSummitComments(summit_id, 10, new Callback<List<UserSummitCommentModel>>() {

            @Override
            public void success(List<UserSummitCommentModel> c, Response response) {
                comments = new ArrayList<>(c);

                if (iterator < comments.size()) {
                    getUser(comments.get(iterator).getUser_id());
                } else {

                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
                Toast.makeText(SummitDetails.this, "Connection error", Toast.LENGTH_SHORT);
            }
        });
    }


    private void getUser(int user_id){

        Helper.getPeakApi(this).getSingleUser(user_id, new Callback<UserModel>() {

            @Override
            public void success(UserModel user, Response response) {
                if (image != null) {
                    users.add(user);
                } else {
                    users.add(new UserModel());
                }

                commentContainers.add(new CommentContainer(comments.get(iterator).getTitle(), comments.get(iterator).getText(), users.get(iterator).getImage() ));
                iterator++;

                if (iterator < comments.size()) {
                    getUser(comments.get(iterator).getUser_id());
                } else {
                    CommentAdapter adapter = new CommentAdapter(SummitDetails.this, commentContainers);
                    ListView commentListView = (ListView) findViewById(R.id.detailsCommentListView);
                    commentListView.setAdapter(adapter);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.getLocalizedMessage();
                Toast.makeText(SummitDetails.this, "Connection error", Toast.LENGTH_SHORT);
            }
        });

    }
}