package com.nwh.peak;

import java.io.Serializable;

public class ImageWrapper implements Serializable {
    private Image[] images;

    public ImageWrapper(Image[] images) {
        this.images = images;
    }

    public Image[] getImages() {
        return this.images;
    }

}