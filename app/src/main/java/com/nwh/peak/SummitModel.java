package com.nwh.peak;

import android.location.Location;

import java.util.ArrayList;

public class SummitModel {

    private int id;
    private String uuid;
    private String name;
    private float lat;
    private float lng;
    private String description;
    private int height;
    private float difficulty;
    private float rating;
    private String created_at;
    private String updated_at;
    private int user_id;
    private int distance;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDistance(){
        double lat = GPSTracker.getLatitude();
        double lng = GPSTracker.getLongitude();
        float[] results = new float[3];

        Location.distanceBetween(
                lat,
                lng,
                getLat(),
                getLng(),
                results);

        return (int)(results[0] / 1000.0f);

    }
}

