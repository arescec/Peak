package com.nwh.peak;

/**
 * Created by Administrator on 23.4.2016..
 */
public class RouteGeopointModel {

    private int id;
    private String uuid;
    private String lat;
    private String lng;
    private String updated_at;
    private String created_at;
    private int summit_route_id;
    private int summit_route_summit_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getSummit_route_id() {
        return summit_route_id;
    }

    public void setSummit_route_id(int summit_route_id) {
        this.summit_route_id = summit_route_id;
    }

    public int getSummit_route_summit_id() {
        return summit_route_summit_id;
    }

    public void setSummit_route_summit_id(int summit_route_summit_id) {
        this.summit_route_summit_id = summit_route_summit_id;
    }

}
